<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class MostNokController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
        $year = $this->get('year');

        if (isset($_GET['year'])) {
          $year = $_GET['year'];
        } else {
          $year = date('Y');
        }

    
	    $huawei = $this->db->query('SELECT COUNT(regional) AS count_regional, regional 
        FROM bot_huawei 
        WHERE SUBSTRING(endtime,6,2) = SUBSTRING(NOW(),6,2) AND changeid="REMEDY NOK" 
        GROUP BY regional')->result();
  
      $this->response($huawei, 200);
      
    }
}