<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ReportController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
      $year = $this->get('year');

      if (isset($_GET['year'])) {
        $year = $_GET['year'];
      } else {
        $year = date('Y');
      }

      $huawei = $this->db->query("SELECT bot_huawei.regional AS regional, 
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '01'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS January,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '02'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS February,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '03'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS March,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '04'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS April,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '05'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS May,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '06'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS June,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '07'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS July,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '08'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS August,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '09'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS September,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '10'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS October,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '11'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS November,
                                  CASE
                                    WHEN SUBSTRING(bot_huawei.endtime,6,2) = '12'
                                     THEN COUNT(bot_huawei.changeid)
                                    ELSE 0
                                  END AS December
                                  FROM bot_huawei WHERE YEAR(bot_huawei.endtime) = ".$year." AND bot_huawei.changeid = 'REMEDY NOK' 
                                    GROUP BY bot_huawei.regional")->result();
  
      $this->response($huawei, 200);
    }

    function index_post(){
        $posted = $this->input->post();
        $huawei = $this->input->post();
        foreach ($huawei as $post) {
          $data = array(
              'BTS_NAME'  => $posted['btsname'][$post],
              'USERNAME'  => $posted['username'][$post],
              'REGIONAL'  => $posted['regional'][$post],
              'CHANGE_ID' => $posted['changeid'][$post],
              'RESULT' 	  => $posted['result'][$post],
              'END_TIME'  => $posted['endtime'][$post]
          );

          if($insert){
              $this->response($data,200);
          } else{
              $this->response(array('status' => 'fail', 502));
          }

          $i++;
        }
    }
}