<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class MostUserController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_get() {
      $regional = $this->get('regional');
  	
      if (isset($_GET['month'])) {
        $month = $_GET['month'];
      }

    
	      $detail = $this->db->query('SELECT COUNT(username) 
        AS usernameReg , username
        -- SUBSTRING(username, 1, LENGTH(username) - 7) AS username
        FROM bot_huawei 
        WHERE SUBSTRING(bot_huawei.endtime,6,2) = SUBSTRING(NOW(),6,2) 
        AND YEAR(bot_huawei.endtime) = LEFT(endtime,4)
        AND changeid = "REMEDY NOK"
        -- group by regional 
        group by SUBSTRING(username, 1, LENGTH(username) - 7) 
        ')->result();
      

      $this->response($detail, 200);
    }
}